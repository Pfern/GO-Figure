#!/usr/bin/env python3
import sys
from collections import defaultdict

direct_parent_dict = defaultdict(set)
for line in open(sys.argv[1]):
	if line.startswith('id: GO'):
		go = line.strip().split(': ')[1]
		alt_id_set = set()
	elif line.startswith('is_a: GO'):
		is_a = line.strip().split(': ')[1].split(' ')[0]
		direct_parent_dict[go].add(is_a)
		for alt_id in alt_id_set:
			direct_parent_dict[alt_id].add(is_a)
	elif line.startswith('part_of: GO'):
		part_of = line.strip().split(': ')[1].split(' ')[0]
		direct_parent_dict[go].add(part_of)
		for alt_id in alt_id_set:
			direct_parent_dict[alt_id].add(part_of)
	elif line.startswith('intersect_of: is_a GO:'):
		is_a = line.strip().split(': ')[1].split(' ')[0]
		direct_parent_dict[go].add(is_a)
		for alt_id in alt_id_set:
			direct_parent_dict[alt_id].add(is_a)
	elif line.startswith('intersect_of: part_of GO:'):
		part_of = line.strip().split(': ')[1].split(' ')[0]
		direct_parent_dict[go].add(part_of)
		for alt_id in alt_id_set:
			direct_parent_dict[alt_id].add(part_of)
	elif line.startswith('alt_id: GO:'):
		alt_id = line.strip().split(': ')[1]
		alt_id_set.add(alt_id)
	elif line.startswith('data-version: '):
		obo_version = line.strip().split('data-version: ')[1]
		print('#go.obo version used: '+obo_version)

parent_dict = defaultdict(set)
bool = True
while bool == True:
	bool = False
	for go,parents in direct_parent_dict.items():
		for parent in parents:
			if not parent in parent_dict[go]:
				parent_dict[go].add(parent)
				bool = True
			parents_parents = parent_dict[parent]
			for parents_parent in parents_parents:
				if not parents_parent in parent_dict[go]:
					parent_dict[go].add(parents_parent)
					bool = True

for go,parents in parent_dict.items():
	for parent in parents:
		print(go+'\t'+parent)
